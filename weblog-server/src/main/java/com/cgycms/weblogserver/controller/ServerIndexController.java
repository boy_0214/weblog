package com.cgycms.weblogserver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName : ServerController
 * @Description : 前端交互
 * @Author : cgy
 * @Date: 2020-09-14 16:57
 */
@Controller
public class ServerIndexController {


    /**
     * 加载首页
     *
     * @return
     */
    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "index";
    }

    /*@PostMapping(value = {"/", "/index"})
    @ResponseBody
    public String login() {
        return "请登录!";
    }*/
}
