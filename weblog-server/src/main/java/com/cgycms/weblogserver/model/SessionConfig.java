package com.cgycms.weblogserver.model;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName : SessionConfig
 * @Description : 用户设置
 * @Author : cgy
 * @Date: 2020-09-15 12:47
 */
public class SessionConfig implements Serializable {

    private static final long serialVersionUID = -8191640400484155111L;

    //展示来源数据
    private List<String> source;

    //是否展示IP
    private boolean showIp;

    //是否展示文件名称
    private boolean showFileName;

    //是否展示日志
    private boolean showContent;

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public boolean isShowIp() {
        return showIp;
    }

    public void setShowIp(boolean showIp) {
        this.showIp = showIp;
    }

    public boolean isShowFileName() {
        return showFileName;
    }

    public void setShowFileName(boolean showFileName) {
        this.showFileName = showFileName;
    }

    public boolean isShowContent() {
        return showContent;
    }

    public void setShowContent(boolean showContent) {
        this.showContent = showContent;
    }

    public SessionConfig(List<String> source, boolean showIp, boolean showFileName, boolean showContent) {
        this.source = source;
        this.showIp = showIp;
        this.showFileName = showFileName;
        this.showContent = showContent;
    }

    public SessionConfig() {
    }

    @Override
    public String toString() {
        return "SessionConfig{" +
                "source=" + source +
                ", showIp=" + showIp +
                ", showFileName=" + showFileName +
                ", showContent=" + showContent +
                '}';
    }
}
