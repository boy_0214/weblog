package com.cgycms.weblogserver.model;

import java.io.Serializable;

/**
 * 封装json结果集
 */
public class JsonResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Boolean success = false;          // 返回是否成功
    private String msg = "";                  // 返回信息
    private T obj = null;                // 返回其他对象信息
    private String type;

    private String detailMsg = ""; // 错误详情

    public JsonResult() {
    }

    public JsonResult(Boolean success, String msg, String type) {
        this.success = success;
        this.msg = msg;
        this.type = type;
    }

    public JsonResult(Boolean success, String msg, T obj, String type) {
        this.success = success;
        this.msg = msg;
        this.obj = obj;
        this.type = type;
    }

    public JsonResult(Boolean success, String msg, T obj, String type, String detailMsg) {
        this.success = success;
        this.msg = msg;
        this.obj = obj;
        this.type = type;
        this.detailMsg = detailMsg;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetailMsg() {
        return detailMsg;
    }

    public void setDetailMsg(String detailMsg) {
        this.detailMsg = detailMsg;
    }

    // ==========  static methods

    public static <T> com.cgycms.weblogserver.model.JsonResult<T> resultError() {
        return resultError("操作失败", null);
    }


    public static <T> com.cgycms.weblogserver.model.JsonResult<T> resultError(String message, String type) {
        return new com.cgycms.weblogserver.model.JsonResult<T>(false, message, type);
    }

    public static <T> com.cgycms.weblogserver.model.JsonResult<T> createErrorMsg(String message, String detailMsg) {
        return new com.cgycms.weblogserver.model.JsonResult<T>(false, message, detailMsg);
    }

    public static <T> com.cgycms.weblogserver.model.JsonResult<T> resultSuccess() {
        return resultSuccess("操作成功", null, null);
    }

    public static <T> com.cgycms.weblogserver.model.JsonResult<T> resultSuccess(T obj, String type) {
        return resultSuccess("操作成功", obj, type);
    }

    public static <T> com.cgycms.weblogserver.model.JsonResult<T> resultSuccess(String message, T obj, String type) {
        return new com.cgycms.weblogserver.model.JsonResult<T>(true, message, obj, type);
    }


}
