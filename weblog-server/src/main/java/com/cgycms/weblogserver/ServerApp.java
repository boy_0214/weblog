package com.cgycms.weblogserver;

import com.cgycms.weblogserver.netty.NettyServerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName : ServerApp
 * @Description : 日志收集工具服务端启动类
 * @Author : cgy
 * @Date: 2020-09-11 15:41
 */
@SpringBootApplication
public class ServerApp implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(ServerApp.class, args);
    }


    @Autowired
    private NettyServerListener nettyServerListener;

    @Override
    public void run(String... args) throws Exception {
        nettyServerListener.start();
    }
}
