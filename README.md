# WEBLOG

##常见问题
有很多人后台私信，邮件；平时很少登录查看，特此创建一个群，方便大家交流。
![输入图片说明](%E5%BC%80%E6%BA%90%E4%BA%A4%E6%B5%81%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)


### 结构
common -- 公共包  
server -- 服务端  
gather -- 客户端

客户端配置扫描日志路径,进行上传;  
服务端接收日志信息,解析后转发前端;  
###  技术栈：  
  1.服务端、客户端基于netty5容器  
  2.服务端展示日志信息基于websocket传输  
  3.文件监听采用IO-FileAlterationListenerAdaptor  
  4.序列化框架基于netty-StringDecoder,StringEncoder及fastJson  
  5.前端基于原生js、jquery、bootstrap4.5 

### 指令： 
  1.需要先启动服务端项目 
  
     nohup java -jar weblog-server.jar >> server.log &  
   
   
 2.客户端配置文件描述 
    
    --gatherConfig 客户端配置：
        serverHost netty服务端IP
        serverPort netty服务端口
        interval 轮训扫描时间(毫秒)
        channelNumber 创建netty通道数量默认1
        retryNumber 客户端发送服务端失败后重试次数默认10次
    --inputs 文件配置
            name 描述名称
            file 文件名称
            path 文件路径
            
   gather.json完整配置：  
      
    { 
     "gatherConfig": {
       "serverHost": "127.0.0.1",
       "serverPort": 9002,
       "interval": 1,
       "channelNumber": 2,
       "retryNumber": 5
     },
     "inputs": [
       {
         "name": "前端网关",
   	     "file": "gate.log",
         "path": [
           "/var/data/log"
         ]
       }
     ]
    }
   
   
   
 3.启动客户端并指定配置文件路径  
  
     nohup java -jar weblog-gather.jar --configPath=gather.json >> gather.log &
   
 4.服务启动成功信息  
 
    serverDefault: port:9000 nettyport:9002
    serverMessage: netty服务器启动成功,监听端口[9002]...  
    gatherDefault: port:8000  
    gatherMessage: Undertow started on port(s) 8000 (http)
    serverHomeUrl: http://127.0.0.1:9000  
    serverHomeMsg: 连接成功
  
  
  ### 其他描述
     
     1.菜单数据来源→客户端检查到日志后描述名称。客户端如果没有启动，或没有输出日志将不会展示菜单。
     2.服务端未启动情况,客户端启动会失败。请先启动服务端。
     
   
  

    
