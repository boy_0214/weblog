package com.cgycms.webloggather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName : GatherApp
 * @Description : 日志收集工具客户端启动类
 * @Author : cgy
 * @Date: 2020-09-11 13:47
 */
@SpringBootApplication
public class GatherApp {

    public static void main(String[] args) {
        SpringApplication.run(GatherApp.class, args);
    }


}
