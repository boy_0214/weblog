package com.cgycms.webloggather.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName : GatherConfig
 * @Description : 读取yml配置文件
 * @Author : Wlc
 * @Date: 2020-09-14 15:21
 */
@Component
public class GatherConfigPath {


    @Value("${configPath}")
    private  String configPath;

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }
}
