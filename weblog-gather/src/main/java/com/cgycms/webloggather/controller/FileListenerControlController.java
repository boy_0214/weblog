package com.cgycms.webloggather.controller;

import com.cgycms.webloggather.file.FileListenerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : FileListenerControlController
 * @Description : 监听文件控制器
 * @Author : Wlc
 * @Date: 2020-09-17 11:35
 */
@RestController
public class FileListenerControlController {


    @Autowired
    private FileListenerFactory fileListenerFactory;


    @GetMapping("/gather/startMonitor")
    public String startMonitor() {
        fileListenerFactory.startMonitor();
        return "开启文件监控服务!";
    }

    @GetMapping("/gather/stopMonitor")
    public String stopMonitor() {
        fileListenerFactory.stopMonitor();
        return "停止文件监控服务!";
    }
}
