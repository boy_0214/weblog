package com.cgycms.weblogcommon.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @ClassName : LocalIpUtil
 * @Description : 本地IP工具
 * @Author : Wlc
 * @Date: 2020-09-16 14:27
 */
@Component
public class LocalIpUtil {

    private static final Logger logger = LoggerFactory.getLogger(LocalIpUtil.class);

    @Autowired
    Environment environment;


    public String getPort() {
        return environment.getProperty("local.server.port");
    }

    public static InetAddress getAddress() {
        InetAddress localHost = null;
        try {
            localHost = Inet4Address.getLocalHost();
        } catch (UnknownHostException e) {
            logger.error(e.getMessage(), e);
        }
        return localHost;
    }


    public static String getIp() {
        return getAddress().getHostAddress();
    }


    public static String getHostName() {
        return getAddress().getHostName();
    }


}
