package com.cgycms.weblogcommon.model.gather;

import java.util.List;


/**
 * @ClassName : GatherConfig
 * @Description : 客户端配置类
 * @Author : Wlc
 * @Date: 2020-09-14 15:21
 */
public class GatherMonitor {

    private GatherConfig gatherConfig;
    private List<GatherInputs> inputs;

    public void setGatherConfig(GatherConfig gatherConfig) {
        this.gatherConfig = gatherConfig;
    }

    public GatherConfig getGatherConfig() {
        return gatherConfig;
    }

    public void setInputs(List<GatherInputs> inputs) {
        this.inputs = inputs;
    }

    public List<GatherInputs> getInputs() {
        return inputs;
    }

}