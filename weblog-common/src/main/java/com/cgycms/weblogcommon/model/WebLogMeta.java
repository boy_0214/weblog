package com.cgycms.weblogcommon.model;

import java.io.Serializable;
import java.util.Map;

/**
 * @ClassName : WebLogMeta
 * @Description : netty传输对象
 * @Author : Wlc
 * @Date: 2020-09-11 16:30
 */
public class WebLogMeta implements Serializable {

    private static final long serialVersionUID = 8379109667714148890L;

    //来源
    private String source;
    //内容
    private String content;
    //IP
    private String ip;
    //文件名称
    private String fileName;
    //其他参数
    private Map<String, Object> parameter;


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return "WebLogMeta{" +
                "source='" + source + '\'' +
                ", content='" + content + '\'' +
                ", ip='" + ip + '\'' +
                ", fileName='" + fileName + '\'' +
                ", parameter=" + parameter +
                '}';
    }

    public WebLogMeta() {
    }

    public WebLogMeta(String source, String content, String ip, String fileName, Map<String, Object> parameter) {
        this.source = source;
        this.content = content;
        this.ip = ip;
        this.fileName = fileName;
        this.parameter = parameter;
    }
}
