package com.cgycms.weblogcommon.model.gather;

import java.util.List;

/**
 * @ClassName : GatherInputs
 * @Description : 客户端配置类
 * @Author : Wlc
 * @Date: 2020-09-14 15:21
 */
public class GatherInputs {

    private String name;
    private String file;
    private List<String> path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }
}