package com.cgycms.weblogcommon.model.gather;

/**
 * @ClassName : GatherConfig
 * @Description : 客户端配置类
 * @Author : Wlc
 * @Date: 2020-09-14 15:21
 */
public class GatherConfig {

    private String serverHost;
    private int serverPort;
    private int interval;
    private int channelNumber;
    private int retryNumber;

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public String getServerHost() {
        return serverHost;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getInterval() {
        return interval;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    public int getRetryNumber() {
        return retryNumber;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }
}