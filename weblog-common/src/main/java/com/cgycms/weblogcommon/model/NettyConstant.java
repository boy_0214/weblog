package com.cgycms.weblogcommon.model;

import org.springframework.stereotype.Component;

/**
 * @ClassName : NettyConstant
 * @Description : netty常量类
 * @Author : Wlc
 * @Date: 2020-09-11 15:56
 */
@Component
public class NettyConstant {


    /**
     * 最大线程量
     */
    private static final int MAX_THREADS = 1024;
    /**
     * 数据包最大长度
     */
    private static final int MAX_FRAME_LENGTH = 65535;

    public static int getMaxThreads() {
        return MAX_THREADS;
    }

    public static int getMaxFrameLength() {
        return MAX_FRAME_LENGTH;
    }
}
